package formation.year2122.epsi.java.repository;

import formation.year2122.epsi.java.model.Power;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PowerRepository extends JpaRepository<Power, Long> {

    @Query("select p from Power p where p.name = ?1")
    List<Power> findAllByName(String name);

    @Query("select p from Power p where p.name like ?1%")
    List<Power> findAllByNameIsStartingWith(String name);
}
